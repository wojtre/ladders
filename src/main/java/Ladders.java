import javafx.util.Pair;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;

/**
 * Created by Wojtek on 16.12.2016.
 */
public class Ladders {
    private Set<String> words = new HashSet<String>();
    private PriorityQueue<Pair<Stack<String>,Integer>> queue = new PriorityQueue<Pair<Stack<String>, Integer>>(new Comparator<Pair<Stack<String>, Integer>>() {


        public int compare(Pair<Stack<String>, Integer> o1, Pair<Stack<String>, Integer> o2) {
            if(o1.getValue() > o2.getValue()){
                return 1;
            } else if(o1.getValue() < o2.getValue()){
                return -1;
            }
            return 0;
        }
    });


    public static void main(String[] args) {
        Ladders ladders = new Ladders();
        Stack<String> ladder = ladders.findLadder(args[0], args[1]);
        saveSolutionToFile(ladder);
    }

    private static void saveSolutionToFile(Stack<String> solution) {
        Path file = Paths.get("output.txt");
        try {
            Files.write(file, solution, Charset.forName("UTF-8"));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    public Stack<String> findLadder(String word, String searchWord) {
        try {
            readFile("wordList.txt");
        } catch (IOException e) {
            e.printStackTrace();
        }
        Iterator<String> iterator = words.iterator();
        while (iterator.hasNext()) {
            String w = iterator.next();
            if (Math.abs(calculateDist(word, w)) == 1) {
                Stack<String> s = new Stack<String>();
                s.add(word);
                s.add(w);
                iterator.remove();
                queue.add(new Pair<Stack<String>, Integer>(s,1 + calculateDist(searchWord,w)));
            }
        }

        while (!queue.isEmpty()) {
            Pair<Stack<String>, Integer> el = queue.remove();
//            System.out.println(queue.size());
            if (el.getKey().peek().equals(searchWord)) {
                return el.getKey();
            } else {
                Iterator<String> iter = words.iterator();
                while (iter.hasNext()) {
                    String w = iter.next();
                    if (Math.abs(calculateDist(el.getKey().peek(), w)) == 1) {
                        Stack<String> s = new Stack<String>();
                        s.addAll(el.getKey());
                        s.add(w);
                        iter.remove();
                        queue.add(new Pair<Stack<String>, Integer>(s,s.size() - 1 + calculateDist(searchWord,w)));
                    }
                }
            }
        }

        return null;
    }

    private int calculateDist(String w1, String w2) {
        List<String> word1 = new LinkedList<String>(Arrays.asList(w1.split("")));
        List<String> word2 = new LinkedList<String>(Arrays.asList(w2.split("")));
        Iterator<String> iter1 = word1.iterator();
        while (iter1.hasNext()) {
            Iterator<String> iter2 = word2.iterator();
            String el1 = iter1.next();
            while (iter2.hasNext()) {
                if (el1.equals(iter2.next())) {
                    iter1.remove();
                    iter2.remove();
                    break;
                }
            }
        }
        return word1.size()+ word2.size();
    }


    private void readFile(String file) throws IOException {
        BufferedReader br = new BufferedReader(new FileReader(new File(file)));

        String line;
        while ((line = br.readLine()) != null) {
            words.add(line);
        }

        br.close();
    }
}
